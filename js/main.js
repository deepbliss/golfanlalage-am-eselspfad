/*dubmenu script*/
$(".nicemenu").nicemenu(); 
   
 /*sticky header*/
        $window = $(window);
        $window.scroll(function() {
          $scroll_position = $window.scrollTop();
            if ($scroll_position > 1) { // if body is scrolled down by 300 pixels
                $('header').addClass('sticky');  
                // to get rid of jerk
                header_height = $('header').innerHeight();
                
            } else {
                $('body').css('padding-top' , '0');
                $('header').removeClass('sticky');
            }
         });
         

 //animate script
         wow = new WOW(
                      {
                      boxClass:     'wow',      // default
                      animateClass: 'animated', // default
                      offset:       0,          // default
                      mobile:       true,       // default
                      live:         true        // default
                    }
                    )
        wow.init();
        
 //mmenu
  /*$(document).ready(function() { 
             mobile menu
             $(function() {
            $('#menu').mmenu({
                extensions: ['effect-slide-menu', 'pageshadow'],
                searchfield: false,
                counters: false,
                navbar: {
                    title: 'Menu'
                },
                navbars: [{
                    position: 'top',
                    <!--content : [ 'searchfield' ]-->               
                }, {
                    position: 'top',
                    content: [
                        'prev',
                        'title',
                        'close'
                    ]
                }]
            });
        });        
        
            }); */

    $(document).ready(function() { 
            /* mobile menu*/
    
$(function() {
    $('nav#menu').mmenu({
        extensions    : [ 'effect-slide-menu', 'pageshadow' ],
        searchfield    : false,
        counters    : false,
        
        navbars        : [
            {
                position    : 'top',
                content        : [
                    'prev',
                    'title',
                    'close'
                ]
            }
        ]
    });
});
        });

/*left menu*/

jQuery('.header-menu .dropdown').hover(hoverin, hoverout);

function hoverin() {
 jQuery(this).children('.header-menu  .dropdown .dropdown-menu').slideDown(500);
}

function hoverout() {
 jQuery(this).children('.header-menu  .dropdown .dropdown-menu').slideUp(500);
}


/*right menu*/
jQuery('header .report .dropdown').hover(hoverin1, hoverout1);

function hoverin1() {
 jQuery(this).children('header .report  .dropdown .dropdown-menu').slideDown(500);
}

function hoverout1() {
 jQuery(this).children('header .report  .dropdown .dropdown-menu').slideUp(500);
}

jQuery(".btn-outline-success").click(function(){
jQuery(".header-search .block-search").fadeToggle(); 
}); 
jQuery(".search-close").click(function(){
jQuery(".header-search .block-search").css("display", "none");
});    
